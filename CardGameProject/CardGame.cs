﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalCardGame
{
    struct GameScore
    {
        //Use automatic properties instead of field variables in the structure
        //private int _playerScore;
        //private int _houseScore;

        //Because there is no busines logic associated with the score use 
        //automatic properties. AUTOMATIC properties do not use explicit field variables
        public int PlayerScore { get; set;}
        public int HouseScore { get; set; }

        public GameScore(int playerScore, int houseScore)
        {
            this.PlayerScore = playerScore;
            this.HouseScore = houseScore;
        }
    }

    class CardGame
    {
        private CardDeck _deck;

        //TODO: define read-only properties for these field variables
        private GameScore _score;

        private Card _playerCard;

        private Card _houseCard;

        //TODO: Define the constructor method for the CardGame class
        public CardGame()
        {
            //Initialize ALL field variables
            _deck = new CardDeck();
            _score = new GameScore();
            _playerCard = null;
            _houseCard = null;
        }

        public void Play()
        {
            sbyte roundResult = PlayRound();
        }

        /// <summary>
        /// Plays a round in the game
        /// </summary>
        /// <returns>
        /// +1: the player won the round
        /// -1: the house won the round
        /// 0: the round was a draw
        /// </returns>
        public sbyte PlayRound()
        {
            string exchangeInput;

            //determine the ranks of the player and house cards
            byte playerCardRank = DetermineCardRank(_playerCard);
            byte houseCardRank = DetermineCardRank(_houseCard);

            //determine who won the round, the player or the house
            if (playerCardRank > houseCardRank)
            {
                //player won the round
                return 1;
            }
            else if (houseCardRank > playerCardRank)
            {
                //house won the round
                return -1;
            }
            else
            {
                //the round is a draw
                return 0;
            }
        }

        /// <summary>
        /// Determines the rank of the card such that Ace has the highest rank
        /// </summary>
        /// <param name="card"></param>
        /// <returns>
        ///   14: if the value is an Ace
        ///   value of the card for any other card
        /// </returns>
        public byte DetermineCardRank(Card card)
        {
            //check for Ace to ensure it has the highest rank
            return (card.Value == 1) ? (byte)14 : card.Value;

            ////check for Ace to ensure it has the highest rank
            //if (card.Value == 1)
            //{
            //    //This is the Ace card, return highest rank
            //    return 14;
            //}
            //else
            //{
            //    //Regular card, the rank is the same as the value
            //    return card.Value;
            //}
        }
    }
}
